core = 7.x
api = 2

; Modules
projects[admin_menu][subdir] = contrib
projects[ctools][subdir] = contrib
projects[devel][subdir] = contrib
projects[features][subdir] = contrib
projects[generate_errors][subdir] = contrib
projects[module_filter][subdir] = contrib
projects[views][subdir] = contrib
projects[i18n][subdir] = contrib
projects[l10n_client][subdir] = contrib
projects[l10n_update][subdir] = contrib
projects[stringoverrides][subdir] = contrib
projects[entity_translation][subdir] = contrib
projects[title][subdir] = contrib
projects[i18_page_views][subdir] = contrib
projects[i18nviews][subdir] = contrib
projects[panels][subdir] = contrib
projects[globalredirect][subdir] = contrib
projects[metatag][subdir] = contrib
projects[page_title][subdir] = contrib
projects[pathauto][subdir] = contrib
projects[redirect][subdir] = contrib
projects[transliteration][subdir] = contrib
